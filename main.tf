terraform {
  required_version = ">= 1.0.1"
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

locals {
  commands = {
    enable_ssl  = [
      "--entrypoints.websecure.address=:443",
      "--entrypoints.web.http.redirections.entrypoint.to=websecure",
      "--entrypoints.web.http.redirections.entrypoint.scheme=https",
      "--entrypoints.web.http.redirections.entrypoint.permanent=true",
      "--entrypoints.websecure.http.tls=true",
      "--entrypoints.websecure.http.tls.certResolver=letsencrypt",
      "--certificatesResolvers.letsencrypt.acme.httpChallenge=true",
      "--certificatesResolvers.letsencrypt.acme.httpChallenge.entryPoint=web",
      "--certificatesResolvers.letsencrypt.acme.email=nerdjacuzzi@posteo.de",
      "--certificatesResolvers.letsencrypt.acme.storage=/letsencrypt/acme.json",
    ],
    disable_ssl = []
  }
}

resource docker_container proxy {
  command = concat(
    var.enable_ssl ? local.commands.enable_ssl : local.commands.disable_ssl,
    [
      "--api=true",
      "--api.insecure=true",
      "--api.dashboard=true",
      "--providers.docker=true",
      "--providers.docker.exposedbydefault=false",
      "--log.level=DEBUG",
      "--entryPoints.web.address=:80",
    ],
    var.proxy.entrypoints
  )

  image = var.proxy.image
  name  = var.proxy.name

  networks_advanced {
    name = var.proxy.network
  }
  dynamic networks_advanced {
    for_each = var.networks_advanced
    content {
      name = networks_advanced.value.networks_advanced
    }
  }
  dynamic ports {
    for_each = var.ports
    content {
      internal = ports.value.external
      external = ports.value.internal
    }
  }
  dynamic volumes {
    for_each = var.enable_ssl ? [1] : []
    content {
      read_only       = false
      container_path  = "/letsencrypt"
      host_path       = "/opt/traefik"
    }
  }
  labels {
    label = "traefik.enable"
    value = "true"
  }
  labels {
    label = "traefik.docker.network"
    value = var.proxy.network
  }
  labels {
    label = "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme"
    value = var.enable_ssl ? "https" : "http"
  }
  labels {
    label = "traefik.http.routers.dashboard.rule"
    value = "Host(`localhost`) && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))"
  }
  labels {
    label = "traefik.http.routers.dashboard.service"
    value = "api@internal"
  }
  volumes {
    container_path  = "/var/run/docker.sock"
    host_path       = "/var/run/docker.sock"
    read_only       = true
  }
}
