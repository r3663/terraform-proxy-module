variable proxy {
  type = object({
    name  = string
    image = string
    network = string
    entrypoints = list(string)
  })
  description = "traefik configuration parameters"
}

variable ports {
  type    = map(object({
    external = number
    internal = number
  }))
  default = {
    "http" = {
      internal = "80"
      external = "80"
    }
    "https" = {
      internal = "443"
      external = "443"
    }
  }
  description = "the ports traefik should bind to."
}

variable networks_advanced {
  type    = map(object({
    name = string
  }))
  default = {}
  description = "the used docker network e.g. traefik-network. This needs to be created by this module and needs to be created prior to deployments"
}

variable enable_ssl {
  type    = bool
  default = false
  description = "enable the ssl entrypoint for traefik"
}
